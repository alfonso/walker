Making a real QWOP: Something the early internet showed us is that walking consciously is really hard. Give it a try here : [QWOP](http://www.foddy.net/Athletics.html)

# **Building Block Legs**

<img src="img/main.png" alt="scheme" width = "900px">


<p>
Learning to walk is weird.
</p>
<img src="img/walk1.gif" alt="scheme" width = "300px">

I decided to start designing and building a first prototype of my attempt of making a bunch of voxel architecture to walk.

To do so, I decided to simplify the legs articulation till the hips to the toes by making the following approximation.



<img src="img/scheme.png" alt="scheme" width = "700px">





Each articulation is actuated with a servomotor. Im thinking about use on the hips some stepper motor due to this continuing stalling position the servos seems to be at.


Chris told me about this rotating device and I decided to design this circular platform that will allow the robot to walk with counterweights but also Im able to control all the DOF that I want the robot to deal with. For now the robot needs to handle the circular motion and altitude, but rotations can be set free as soon as I am doing this in a close loop. 


<img src="img/walkstatic.gif" alt="scheme" width = "300px">
<img src="img/walkstatic1.gif" alt="scheme" width = "300px">
